using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour
{
    Rigidbody2D rB2D;

    public SpriteRenderer spriteRenderer;
    public Animator anim;

    public GameObject instructions;
    public GameObject instructionsbackground;

    public float runSpeed;

    public float jumpSpeed;

    bool shouldJump;

    // Start is called before the first frame update
    void Start()
    {
        rB2D = GetComponent<Rigidbody2D>();

        instructions.SetActive(true);
        instructionsbackground.SetActive(true);
        Time.timeScale = 0f;
    }

    private void FixedUpdate() 
    {
        float horizontalInput = Input.GetAxis("Horizontal");

        rB2D.velocity = new Vector2(horizontalInput * runSpeed * Time.deltaTime,  rB2D.velocity.y);

        if(shouldJump)
        {
             rB2D.velocity = new Vector2(rB2D.velocity.x, rB2D.velocity.y + jumpSpeed);
             shouldJump = false;
        }

        if( rB2D.velocity.x > 0 ) 
        {
            spriteRenderer.flipX = false;
        }
        else
        {
            spriteRenderer.flipX = true;
        }

        if( Mathf.Abs(horizontalInput) > 0f)
        {
            anim.SetBool("isRunning", true);
        }
        else
        {
            anim.SetBool("isRunning", false);
        }

    }

    // Update is called once per frame
    void Update()
    {
        if(Input.GetKeyDown( KeyCode.Space ) ) 
        {
            int levelMask = LayerMask.GetMask("Level", "Box");
            Debug.Log("Pressed Space");
//if you want air jumping then remove this. 
            RaycastHit2D hit = Physics2D.BoxCast( transform.position, new Vector2( 1f, 1f ), 0f, Vector2.down, 1f, levelMask ) ;
           // Debug.Log(hit.collider.gameObject.name);
            if( Physics2D.BoxCast( transform.position, new Vector2( 1f, 1f ), 0f, Vector2.down, 2f, levelMask ) )  {
                     Debug.Log("Should jump");
                     shouldJump = true;
            }
           
        }

        if(Input.GetKey(KeyCode.LeftShift))
        {
            instructions.SetActive(false);
            instructionsbackground.SetActive(false);

            Time.timeScale = 1f;
        }
    }
}
