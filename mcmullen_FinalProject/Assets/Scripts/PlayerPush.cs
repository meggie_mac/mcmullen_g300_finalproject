using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerPush : MonoBehaviour
{
    public float distance=1f;
	public LayerMask boxMask;

    public SpriteRenderer spriteRenderer;   

	GameObject box;

    public int lineFlip = 0;

	// Use this for initialization
	void Start () 
    {
        
	}
	
	// Update is called once per frame
	void Update () {
	
                if (spriteRenderer.flipX)
                {
                    lineFlip = 1;
                }
                else
                {
                    lineFlip = -1;
                }

				Physics2D.queriesStartInColliders = false;
				//RaycastHit2D hit= Physics2D.Raycast (transform.position, Vector2.right * transform.localScale.x, distance, boxMask);
                RaycastHit2D hit= Physics2D.Raycast (transform.position, Vector2.left * lineFlip, distance, boxMask);

				if (hit.collider != null && hit.collider.gameObject.tag == "pushable" && Input.GetKey (KeyCode.E)) 
                {
						box = hit.collider.gameObject;
						box.GetComponent<FixedJoint2D> ().connectedBody = this.GetComponent<Rigidbody2D> ();
						box.GetComponent<FixedJoint2D> ().enabled = true;
						//box.GetComponent<boxpull> ().beingPushed = true;

				} 
                else if (Input.GetKeyUp (KeyCode.E))
                {
                    if (box != null)
                    {
						box.GetComponent<FixedJoint2D> ().enabled = false;
						//box.GetComponent<boxpull> ().beingPushed = false;
                    }
				}

		
		}


		void OnDrawGizmos()
		{
				Gizmos.color = Color.yellow;

				//Gizmos.DrawLine (transform.position, (Vector2)transform.position + Vector2.right * transform.localScale.x * distance);
                Gizmos.DrawLine (transform.position, (Vector2)transform.position + Vector2.left * lineFlip * distance);
		
		
		}
}

